package com.hireright.parsers;

import com.hireright.models.ProcessingCommand;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.*;

public class ArgumentParser {
    private List<String> resourceURLList;
    private List<String> wordList;
    private Set<ProcessingCommand> commandSet;

    public ArgumentParser(String[] args) {
        if (args.length < 3) {
            throw new RuntimeException("Please, specify 3 arguments at least (source, words and commands)");
        }
        List<String> argumentList = new ArrayList<String>(Arrays.asList(args));
        String resourceListArg = argumentList.remove(0);
        String wordListArg = argumentList.remove(0);

        if (isURL(resourceListArg)) {
            resourceURLList = new ArrayList<String>(Arrays.asList(resourceListArg));
        } else {
            resourceURLList = readURLListFromFile(resourceListArg);
        }
        wordList = Arrays.asList(wordListArg.split(","));
        commandSet = new HashSet<ProcessingCommand>();
        if(argumentList.contains("–w")){
         commandSet.add(ProcessingCommand.WORDCOUNT);
        }
        if(argumentList.contains("–c")){
            commandSet.add(ProcessingCommand.CHARCOUNT);
        }
        if(argumentList.contains("–e")){
            commandSet.add(ProcessingCommand.EXTRACT);
        }
        if(argumentList.contains("–v")){
            commandSet.add(ProcessingCommand.VERBOSE);
        }
    }

    public List<String> getResourceURLList() {
        return resourceURLList;
    }

    public List<String> getWordList() {
        return wordList;
    }

    public Set<ProcessingCommand> getCommandSet() {
        return commandSet;
    }

    private boolean isURL(String address) {
        boolean result = false;
        try {
            URL url = new URL(address);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("HEAD");
            connection.connect();
            result = connection.getResponseCode() == 200;
        } catch (ProtocolException e) {
            return false;
        } catch (MalformedURLException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
        return result;
    }

    private List<String> readURLListFromFile(String path) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(path));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(String.format("File %s is not found", path));
        }
        ArrayList<String> urlList = new ArrayList<String>();
        while (scanner.hasNextLine()){
            urlList.add(scanner.nextLine());
        }
        scanner.close();
        return urlList;
    }


}
