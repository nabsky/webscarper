package com.hireright.output;

import com.hireright.models.ProcessedData;
import com.hireright.models.ScarpedData;

import java.util.Map;

public interface ResultOutput {
    void out(Map<ScarpedData, ProcessedData> resultMap, boolean debugMode);
}
