package com.hireright.output.impl;

import com.hireright.models.ProcessedData;
import com.hireright.models.ScarpedData;
import com.hireright.models.SummarizedData;
import com.hireright.output.ResultOutput;
import com.hireright.processors.ResultProcessor;
import com.hireright.processors.impl.HireRightResultProcessor;

import java.util.List;
import java.util.Map;

public class ConsoleResultOutput implements ResultOutput {
    public static final String ALIGNED_FORMAT = "%-30s %8d";

    public void out(Map<ScarpedData, ProcessedData> resultMap, boolean debugMode) {

        //TODO DRY
        //TODO pass command set instead of checking results for null
        for (Map.Entry<ScarpedData, ProcessedData> entry : resultMap.entrySet()) {
            ScarpedData scarpedData = entry.getKey();
            ProcessedData processedData = entry.getValue();
            System.out.println(String.format("Results for %s:", scarpedData.getSource()));
            displayCharacterCount(processedData.getCharacterCount());
            displayWordOccurrence(processedData.getWordsCountMap());
            displayExtractedSentences(processedData.getExtractedSentenceMap());
            if (debugMode) {
                displayDebugInfo(scarpedData.getScarpingTimeInMilliseconds(), processedData.getProcessingTimeInMilliseconds());
            }

        }

        ResultProcessor resultProcessor = new HireRightResultProcessor();
        SummarizedData summarizedData = resultProcessor.summarize(resultMap);
        System.out.println(String.format("=======================================\nSummarized data for %d source(s):", resultMap.entrySet().size()));
        displayCharacterCount(summarizedData.getCharacterCount());
        displayWordOccurrence(summarizedData.getWordsCountMap());
        if (debugMode) {
            displayDebugInfo(summarizedData.getScarpingTimeInMilliseconds(), summarizedData.getProcessingTimeInMilliseconds());
        }
    }

    private void displayCharacterCount(Long characterCount) {
        if (characterCount != null) {
            System.out.println(String.format(ALIGNED_FORMAT, "Total characters count", characterCount));
        }
    }

    private void displayWordOccurrence(Map<String, Long> wordCountMap) {
        if (wordCountMap != null) {
            System.out.println("Word occurrences information:");
            for (Map.Entry<String, Long> wordEntry : wordCountMap.entrySet()) {
                String word = wordEntry.getKey();
                Long count = wordEntry.getValue();
                System.out.println(String.format(ALIGNED_FORMAT, word, count));
            }
        }
    }

    private void displayDebugInfo(Long scarpingTime, Long processingTime) {
        System.out.println("Debug info:");
        System.out.println(String.format(ALIGNED_FORMAT, "Scarping time (ms)", scarpingTime));
        System.out.println(String.format(ALIGNED_FORMAT, "Processing time (ms)", processingTime));
    }

    private void displayExtractedSentences(Map<String, List<String>> sentencesMap){
        if(sentencesMap != null){
            System.out.println("Sentences extraction will be implemented in future version. Stay in touch =)");
        }
    }
}
