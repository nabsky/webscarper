package com.hireright.scarpers;

import com.hireright.models.ScarpedData;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

public interface DataScarper {
    List<ScarpedData> scarp(List<String> sourceList);
}
