package com.hireright.scarpers.impl;

import com.hireright.models.ScarpedData;
import com.hireright.scarpers.DataScarper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class URLDataScarper implements DataScarper {

    public List<ScarpedData> scarp(List<String> sourceList) {
        List<ScarpedData> scarpedDataList = new ArrayList<ScarpedData>();
        for (String source : sourceList) {
            Long startTime = new Date().getTime();
            String rawData = null;
            try {
                URL resourceURL = new URL(source);
                BufferedReader in = new BufferedReader(new InputStreamReader(resourceURL.openStream()));
                String inputLine;
                StringBuilder stringBuilder = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    stringBuilder.append(inputLine);
                }
                in.close();
                rawData = stringBuilder.toString();
            } catch (Exception e) {
                //TODO mark as failed and display in results
            }
            Long endTime = new Date().getTime();
            ScarpedData scarpedData = new ScarpedData();
            scarpedData.setSource(source);
            scarpedData.setRawData(rawData);
            scarpedData.setCleanText(clean(rawData));
            scarpedData.setScarpingTimeInMilliseconds(endTime - startTime);
            scarpedDataList.add(scarpedData);
        }
        return scarpedDataList;
    }

    public String clean(String rawData){
        //TODO clean text from js and HTML tags
        return rawData;
    }

}
