package com.hireright.processors;

import com.hireright.models.ProcessedData;
import com.hireright.models.ScarpedData;
import com.hireright.models.SummarizedData;

import java.util.Map;

public interface ResultProcessor {
    SummarizedData summarize(Map<ScarpedData, ProcessedData> resultMap);
}
