package com.hireright.processors.impl;

import com.hireright.models.ProcessedData;
import com.hireright.models.ProcessingCommand;
import com.hireright.models.ScarpedData;
import com.hireright.processors.DataProcessor;

import java.util.*;

public class HireRightDataProcessor implements DataProcessor {

    public Map<ScarpedData, ProcessedData> process(List<ScarpedData> dataList, List<String> wordList, Set<ProcessingCommand> commandSet) {
        Map<ScarpedData, ProcessedData> resultMap = new HashMap<ScarpedData, ProcessedData>();
        for (ScarpedData data: dataList) {
            ProcessedData result = new ProcessedData();
            Long startTime = new Date().getTime();
            if(commandSet.contains(ProcessingCommand.CHARCOUNT)){
                result.setCharacterCount(getCharactersCount(data.getCleanText()));
            }
            if(commandSet.contains(ProcessingCommand.WORDCOUNT)){
                Map<String, Long> wordOccurrenceMap = new HashMap<String, Long>();
                for (String word: wordList) {
                    wordOccurrenceMap.put(word, getWordOccurrence(data.getCleanText(), word));
                }
                result.setWordsCountMap(wordOccurrenceMap);
            }
            if(commandSet.contains(ProcessingCommand.EXTRACT)){
                Map<String, List<String>> sentencesWithWordMap = new HashMap<String, List<String>>();
                for (String word: wordList) {
                    sentencesWithWordMap.put(word, getSentencesWithWordList(data.getCleanText(), word));
                }
                result.setExtractedSentenceMap(sentencesWithWordMap);
            }
            Long endTime = new Date().getTime();
            result.setProcessingTimeInMilliseconds(endTime - startTime);
            resultMap.put(data,result);
        }
        return resultMap;
    }

    private Long getCharactersCount(String text){
        if(text == null){
            return 0L;
        } else {
            return (long) text.length();
        }
    }

    private Long getWordOccurrence(String text, String word){
        if(text == null){
            return 0L;
        } else {
            return (long) ((text.length() - text.replace(word, "").length()) / word.length());
        }
    }

    private List<String> getSentencesWithWordList(String text, String word){
        //TODO implement
        return Collections.emptyList();
    }

}

