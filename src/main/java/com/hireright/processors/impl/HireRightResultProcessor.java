package com.hireright.processors.impl;

import com.hireright.models.ProcessedData;
import com.hireright.models.ScarpedData;
import com.hireright.models.SummarizedData;
import com.hireright.processors.ResultProcessor;

import java.util.Map;

public class HireRightResultProcessor implements ResultProcessor {
    public SummarizedData summarize(Map<ScarpedData, ProcessedData> resultMap) {
        SummarizedData result = new SummarizedData();
        for (Map.Entry<ScarpedData, ProcessedData> entry : resultMap.entrySet()) {
            ScarpedData scarpedData = entry.getKey();
            ProcessedData processedData = entry.getValue();
            result.addCharactersCount(processedData.getCharacterCount());
            if (processedData.getWordsCountMap() != null) {
                for (Map.Entry<String, Long> wordEntry : processedData.getWordsCountMap().entrySet()) {
                    String word = wordEntry.getKey();
                    Long count = wordEntry.getValue();
                    result.addWordCount(word, count);
                }
            }
            result.addScarpingTime(scarpedData.getScarpingTimeInMilliseconds());
            result.addProcessingTime(processedData.getProcessingTimeInMilliseconds());
        }
        return result;
    }
}
