package com.hireright.processors;

import com.hireright.models.ProcessedData;
import com.hireright.models.ProcessingCommand;
import com.hireright.models.ScarpedData;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface DataProcessor {
    Map<ScarpedData, ProcessedData> process(List<ScarpedData> dataList, List<String> wordList, Set<ProcessingCommand> commandSet);
}
