package com.hireright.models;

import java.util.HashMap;

public class SummarizedData extends ProcessedData {
    protected Long scarpingTimeInMilliseconds = 0L;

    public Long getScarpingTimeInMilliseconds() {
        return scarpingTimeInMilliseconds;
    }

    public void setScarpingTimeInMilliseconds(Long scarpingTimeInMilliseconds) {
        this.scarpingTimeInMilliseconds = scarpingTimeInMilliseconds;
    }

    public void addCharactersCount(Long count){
        if(characterCount == null){
            characterCount = count;
        } else {
            characterCount += count;
        }
    }

    public void addWordCount(String word, Long count){
        if(wordsCountMap == null){
            wordsCountMap = new HashMap<String, Long>();
        }

        if (wordsCountMap.get(word) == null){
            wordsCountMap.put(word, count);
        } else {
            wordsCountMap.replace(word, wordsCountMap.get(word) + count);
        }
    }

    public void addScarpingTime(Long time){
        if(scarpingTimeInMilliseconds == null){
            scarpingTimeInMilliseconds = time;
        } else {
            scarpingTimeInMilliseconds += time;
        }
    }

    public void addProcessingTime(Long time){
        if(processingTimeInMilliseconds == null){
            processingTimeInMilliseconds = time;
        } else {
            processingTimeInMilliseconds += time;
        }

    }

}
