package com.hireright.models;

import java.util.List;
import java.util.Map;

public class ProcessedData {
    protected Map<String, Long> wordsCountMap;
    protected Map<String, List<String>> extractedSentenceMap;
    protected Long characterCount;
    protected Long processingTimeInMilliseconds;

    public Map<String, Long> getWordsCountMap() {
        return wordsCountMap;
    }

    public void setWordsCountMap(Map<String, Long> wordsCountMap) {
        this.wordsCountMap = wordsCountMap;
    }

    public Map<String, List<String>> getExtractedSentenceMap() {
        return extractedSentenceMap;
    }

    public void setExtractedSentenceMap(Map<String, List<String>> extractedSentenceMap) {
        this.extractedSentenceMap = extractedSentenceMap;
    }

    public Long getCharacterCount() {
        return characterCount;
    }

    public void setCharacterCount(Long characterCount) {
        this.characterCount = characterCount;
    }

    public Long getProcessingTimeInMilliseconds() {
        return processingTimeInMilliseconds;
    }

    public void setProcessingTimeInMilliseconds(Long processingTimeInMilliseconds) {
        this.processingTimeInMilliseconds = processingTimeInMilliseconds;
    }
}
