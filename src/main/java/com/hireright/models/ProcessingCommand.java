package com.hireright.models;

public enum ProcessingCommand {
    WORDCOUNT, CHARCOUNT, EXTRACT, VERBOSE
}
