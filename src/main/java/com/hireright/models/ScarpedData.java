package com.hireright.models;

public class ScarpedData {
    private String source;
    private String rawData;
    private String cleanText;
    private Long scarpingTimeInMilliseconds;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public String getCleanText() {
        return cleanText;
    }

    public void setCleanText(String cleanText) {
        this.cleanText = cleanText;
    }

    public Long getScarpingTimeInMilliseconds() {
        return scarpingTimeInMilliseconds;
    }

    public void setScarpingTimeInMilliseconds(Long scarpingTimeInMilliseconds) {
        this.scarpingTimeInMilliseconds = scarpingTimeInMilliseconds;
    }
}
