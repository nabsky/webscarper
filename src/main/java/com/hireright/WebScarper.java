package com.hireright;

import com.hireright.models.ProcessedData;
import com.hireright.models.ProcessingCommand;
import com.hireright.models.ScarpedData;
import com.hireright.output.ResultOutput;
import com.hireright.output.impl.ConsoleResultOutput;
import com.hireright.parsers.ArgumentParser;
import com.hireright.processors.DataProcessor;
import com.hireright.processors.impl.HireRightDataProcessor;
import com.hireright.scarpers.DataScarper;
import com.hireright.scarpers.impl.URLDataScarper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WebScarper {

    private List<String> urlList;
    private List<String> wordList;
    private Set<ProcessingCommand> commandSet;

    public static void main(String[] args) throws IOException {
        ArgumentParser argumentParser = new ArgumentParser(args);
        List<String> urlList = argumentParser.getResourceURLList();
        List<String> wordList = new ArrayList<String>(argumentParser.getWordList());
        Set<ProcessingCommand> commandSet = argumentParser.getCommandSet();

        WebScarper webScarper = new WebScarper(urlList, wordList, commandSet);
        Map<ScarpedData, ProcessedData> result = webScarper.execute();

        ResultOutput resultOutput = new ConsoleResultOutput();
        resultOutput.out(result, commandSet.contains(ProcessingCommand.VERBOSE));
    }

    public WebScarper(List<String> urlList, List<String> wordList, Set<ProcessingCommand> commandSet) {
        this.urlList = urlList;
        this.wordList = wordList;
        this.commandSet = commandSet;
    }

    public Map<ScarpedData, ProcessedData> execute() {
        DataScarper dataScarper = new URLDataScarper();
        List<ScarpedData> dataList = dataScarper.scarp(urlList);

        DataProcessor dataProcessor = new HireRightDataProcessor();
        return dataProcessor.process(dataList, wordList, commandSet);
    }
}
